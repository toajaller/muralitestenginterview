﻿using System;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestEngInterview;
using VAE.CLI.Flags;

namespace VAE.CLI.Tests
{
    [TestClass]
    public class FlagsTest
    {
        private Parser parser;
        private FlagValue<BigramParser> bigramFlag;

        [TestInitialize]
        public void Initialize()
        {
            parser = new Parser();
            bigramFlag = parser.Add<BigramParser>("bigramFlag",
             new Val<BigramParser>(null,
                 (line) =>
                 {
                     var bigramParser = new BigramParser(new StringReader(line));

                     return Tuple.Create(true, bigramParser);
                 }));
        }


        [TestMethod]
        public void Should_ParseSuccessfully_When_InputValid()
        {
            // Arrange
            var args = new string[] { "--bigramFlag", "The quick brown fox and the quick blue hare." };

            // Act
            parser.Parse(args);

            // Assert
            Assert.AreEqual(parser.HasErrors, false);
            Assert.AreEqual(bigramFlag.Value.ParsedItems.First(pi => pi.Bigram == "the quick").Count, 2);
            Assert.AreEqual(bigramFlag.Value.ParsedItems.First(pi => pi.Bigram == "quick brown").Count, 1);
            Assert.AreEqual(bigramFlag.Value.ParsedItems.First(pi => pi.Bigram == "brown fox").Count, 1);
            Assert.AreEqual(bigramFlag.Value.ParsedItems.First(pi => pi.Bigram == "fox and").Count, 1);
            Assert.AreEqual(bigramFlag.Value.ParsedItems.First(pi => pi.Bigram == "and the").Count, 1);
            Assert.AreEqual(bigramFlag.Value.ParsedItems.First(pi => pi.Bigram == "quick blue").Count, 1);
            Assert.AreEqual(bigramFlag.Value.ParsedItems.First(pi => pi.Bigram == "blue hare").Count, 1);
        }

        [TestMethod]
        public void Should_FailToParse_ForMissingArgument()
        {
            // Arrange
            var args = new string[] { "--bigramFlag", string.Empty };

            // Act
            parser.Parse(args);

            // Assert
            Assert.AreEqual(parser.HasErrors, true);
            Assert.AreEqual(bigramFlag.Value, null);
        }
    }
}
