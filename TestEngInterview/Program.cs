﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VAE.CLI.Flags;

namespace TestEngInterview
{
    class Program
    {
        static void Main(string[] args)
        {
            var parser = new Parser();
            parser.AddIntFlag("myintFlag", 5, "An integer flag");
            parser.Add<BigramParser>("bigramFlag", new Val<BigramParser>(null,
                (path) =>
                {
                    // command line example: --bigramFlag c:\junk\bigram.txt
                    // Contents of bigram.txt
                    // The quick brown fox and the quick blue hare.
                    // quick
                    // The quick fox.
                
                    // custom parsing code here

                    var result = false;
                    BigramParser bigramParser = null;
                    try
                    {
                        bigramParser = new BigramParser(new StreamReader(path));
                        if (bigramParser.Result == Result.Success)
                        {
                            foreach (var parsedItem in bigramParser.ParsedItems)
                            {
                                Console.WriteLine(string.Format("\"{0}\" {1}", parsedItem.Bigram, parsedItem.Count));
                            }

                            result = true;
                        }
                    }
                    catch (FileNotFoundException)
                    {
                        Console.WriteLine(string.Format("File [{0}] does not exist.", path));
                    }
                    catch (DirectoryNotFoundException)
                    {
                        Console.WriteLine(string.Format("Directory [{0}] does not exist.", Path.GetDirectoryName(path)));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(string.Format("Failed to parse file [{0}]. Exception: [{1}]", path, ex.Message));
                    }

                    Console.ReadLine();

                    return Tuple.Create(result, bigramParser);
                }, "A Bigram parser flag"));

            parser.Parse(args);

        }
    }
}
