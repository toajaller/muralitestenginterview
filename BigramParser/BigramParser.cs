﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEngInterview
{
    public enum Result
    {
        Success,

        Failure
    }

    public class ParsedItem
    {
        public string Bigram { get; set; }
        public int Count { get; set; }
    }

    public class BigramParser
    {
        public List<ParsedItem> ParsedItems { get; private set; }
        public Result Result { get; private set; }

        public Char Delimiter { get; private set; }

        public BigramParser(TextReader reader, Char delimiter = ' ')
        {
            Delimiter = delimiter;
            Result = Result.Success;
            ParsedItems = new List<ParsedItem>();
            Parse(reader);
        }

        private void Parse(TextReader reader)
        {
            try
            {

                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    var words = line.Trim('.').ToLower().Split(Delimiter);
                    if (words.Count() <= 1)
                    {
                        continue;
                    }

                    int currentWordIndex = 1;
                    foreach (var word in words)
                    {
                        var nextWord = words.Skip(currentWordIndex).FirstOrDefault();
                        if (!string.IsNullOrEmpty(nextWord))
                        {
                            string item = word.Trim() + " " + nextWord.Trim();
                            AddOrUpdateParsedItem(item);
                        }
                        currentWordIndex++;
                    }
                }

                reader.Close();
            }
            catch 
            {
                Result = Result.Failure;

                throw;
            }
        }

        private void AddOrUpdateParsedItem(string item)
        {
            if (!string.IsNullOrEmpty(item))
            {
                var parsedItem = ParsedItems.SingleOrDefault(i => i.Bigram == item);
                if (parsedItem == null)
                {
                    ParsedItems.Add(new ParsedItem { Bigram = item, Count = 1 });
                }
                else
                {
                    parsedItem.Count++;
                }
            }
        }
    }
}
